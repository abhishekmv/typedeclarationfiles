/// <reference types="node" />

declare module "gpu-info-nv" {
  export const exec:any
  export function promiseExec(command:string):Promise<any>
  export const NVIDIA: {
    name: string;
    memClock: string;
    GpuClock: string;
    temp: string;
    usageGpu: string;
    memoryTotal: string;
    memoryFree: string;
    memoryUsed: string;
  };
  
  export function getName(): Promise<string>;
  export function getTemp(): Promise<number>;
  export function getUsage(): Promise<number>;
  export function getMemoryTotal(): Promise<number>;
  export function getMemoryFree(): Promise<number>;
  export function getMemoryUsed(): Promise<number>;
  export function getGpuClock(): Promise<number>;
  export function getMemClock(): Promise<number>;
}

